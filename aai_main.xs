// 1. LOAD UTILITY FUNCTIONS
include "headers/aai_queries.xs";
include "headers/aai_plans.xs";
include "headers/aai_decks.xs";


// 2. LOAD STANDARD ROUTINES
include "routines/autosave.xs";
include "routines/gathererallocation.xs";
include "routines/resignation.xs";
include "routines/defaultdeck.xs";
include "routines/exploration.xs";


void main(void)
{
    if (aiGetGameType() != cGameTypeRandom)
    {
        if (aiGetGameType() == cGameTypeSaved)
        {
            for(player = 1 ; < cNumberPlayers)
            {
                if (kbIsPlayerHuman(player) == false)
                    continue;
                aiChat(player, "Hey "+kbGetPlayerName(player)+", due to some technical limitations, we've decided not to support saved games.");
                aiChat(player, "So I just wanted you to know that this AI is probably going to have weird behavior. Sorry ^_^ -- Alistair");
            }
        }
        else
        {
            for(player = 1 ; < cNumberPlayers)
            {
                if (kbIsPlayerHuman(player) == false)
                    continue;
                aiChat(player, "Hey "+kbGetPlayerName(player)+", this AI wasn't written to support custom scenarios and camptaigns. Chances are it will not work as you expect it to work.");
                aiChat(player, "If you need help with custom AIs, hit me up in the official Discord server of Wars of Liberty ^_^ -- Alistair");
            }
        }
    }
    if (aiTreatyActive())
    {
        for(player = 1 ; < cNumberPlayers)
        {
            if (kbIsPlayerHuman(player) == false)
                continue;
            aiChat(player, "Hey "+kbGetPlayerName(player)+", we decided not to support the Treaty mode due to the mechanisms of the mod.");
            aiChat(player, "This AI will not play like how people are supposed to play in a Treaty game. Sorry ^_^ -- Alistair");
        }
    }
    if (aiGetGameMode() != cGameModeSupremacy)
    {
        for(player = 1 ; < cNumberPlayers)
        {
            if (kbIsPlayerHuman(player) == false)
                continue;
            aiChat(player, "Hey "+kbGetPlayerName(player)+", this AI wasn't written to work in any other game mode than Supremacy. Chances are it will have weird behavior.");
            aiChat(player, "I suggest you to cancel this game and set up another one in Supremacy mode ^_^ -- Alistair");
        }
    }
    if (kbUnitCount(cMyID, cUnitTypeHomeCityWaterSpawnFlag, cUnitStateAlive) >= 1)
    {
        for(player = 1 ; < cNumberPlayers)
        {
            if (kbIsPlayerHuman(player) == false)
                continue;
            aiChat(player, "Hey "+kbGetPlayerName(player)+", I know it's weird but for the moment, the AI doesn't support naval gameplay.");
            aiChat(player, "I'll hopefully be able to fix it in a future patch. That's it, I just wanted you to know that ^_^ -- Alistair");
        }
    }
    
    kbLookAtAllUnitsOnMap();
    kbAreaCalculate();
    aiRandSetSeed(-1);
    aiSetRandomMap(true);
    aiSetWaterMap(true);
    
    if (aiGetWorldDifficulty() == cDifficultyExpert)
        kbSetPlayerHandicap( cMyID, kbGetPlayerHandicap(cMyID) * 1.5);
    
    // Kill all escrows right now. It will make our life much simpler O:)
    kbEscrowSetPercentage(cMilitaryEscrowID, cAllResources, 0.0);
    kbEscrowSetPercentage(cEconomyEscrowID, cAllResources, 0.0);
    kbEscrowSetPercentage(cRootEscrowID, cAllResources, 1.0);
    aiSetAutoGatherEscrowID(cRootEscrowID);
    aiSetAutoFarmEscrowID(cRootEscrowID);
    kbEscrowAllocateCurrentResources();
    
    // Let us handle gatherer allocation ourselves from the script
    aiSetResourceGathererPercentageWeight(cRGPScript, 1.0);
    aiSetResourceGathererPercentageWeight(cRGPCost, 0.0);
    aiNormalizeResourceGathererPercentageWeights();
    
    // Set the initial gatherer allocation so we don't have to wait for the main allocator rule to allocate gatherers
    aiSetResourceGathererPercentage(cResourceTrade, 0.0, false, cRGPScript);
    aiSetResourceGathererPercentage(cResourceFood, 1.0, false, cRGPScript); // The majority of civs go full food at start
    aiSetResourceGathererPercentage(cResourceWood, 0.0, false, cRGPScript); // The exotic civs (such as Indians and Dutch) will
    aiSetResourceGathererPercentage(cResourceGold, 0.0, false, cRGPScript); // be given specific functions to override this setting
    aiSetResourceGathererPercentage(cResourceFame, 0.0, false, cRGPScript);
    aiSetResourceGathererPercentage(cResourceXP, 0.0, false, cRGPScript);
    aiNormalizeResourceGathererPercentages(cRGPScript);
    
    aiSetHandler("EventResignResponseReceived", cXSResignHandler);
}

