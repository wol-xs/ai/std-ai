/****************************************************************************************************************************************************
    NOTE: this is a header file. Usually, there's no reason to modify header files. Also, there's generally no reason to read header files apart from
    trying to discover and understand the algorithm of certain functions. Be careful if you modify this file.
****************************************************************************************************************************************************/

/****************************************************************************************************************************************************
    Wars of Liberty Standard Header - Queries
    by Thinot "AlistairJah" Mandresy
    Standard Header Files are meant to be reuseable in other AI scripts with minimal modifications.
****************************************************************************************************************************************************/

/****************************************************************************************************************************************************
    Plan functions are used for initiating "planned tasks" (as opposed to "immediate tasks") such as researching techs and training units. Odds are 
    that some of these functions may not suit the modder's needs in some cases. In those cases, the modder will have to manually set up their plans 
    from scratch.
****************************************************************************************************************************************************/


int num_queue_techs = 0;


int planResearch(int tech = -1, int building = -1)
{
    if (kbTechGetStatus(tech) == cTechStatusActive)
        return(-1);
    if (aiPlanGetIDByTypeAndVariableType(cPlanResearch, cResearchPlanTechID, tech, true) >= 0)
        return(-1);
    if (num_queue_techs >= kbGetAge() + 2)
        return(-1);
    int plan = aiPlanCreate("Research "+kbGetTechName(tech), cPlanResearch);
    aiPlanSetEscrowID(plan, cRootEscrowID);
    aiPlanSetVariableInt(plan, cResearchPlanTechID, 0, tech);
    aiPlanSetVariableInt(plan, cResearchPlanBuildingID, 0, building);
    aiPlanSetEventHandler(plan, cPlanEventStateChange, "TechQueueCounter");
    aiPlanSetActive(plan, true);
    return(plan);
}


void TechQueueCounter(int plan = -1)
{
    if (kbTechGetStatus(aiPlanGetVariableInt(plan, cResearchPlanTechID, 0)) == cTechStatusActive)
        return;
    if (aiPlanGetState(plan) == cPlanStateResearch)
        num_queue_techs++;
}


int planMaintain(int unit = -1, int number = 1, bool multibuildings = false, int batch = 1, int source_proto = -1, int maxqueue = 1)
{
    int plan = aiPlanCreate("Maintain "+kbGetProtoUnitName(unit), cPlanTrain);
    aiPlanSetEscrowID(plan, cRootEscrowID);
    aiPlanSetVariableInt(plan, cTrainPlanUnitType, 0, unit);
    aiPlanSetVariableInt(plan, cTrainPlanNumberToMaintain, 0, number);
    aiPlanSetVariableInt(plan, cTrainPlanBatchSize, 0, batch);
    aiPlanSetVariableInt(plan, cTrainPlanBuildFromType, 0, source_proto);
    aiPlanSetVariableInt(plan, cTrainPlanMaxQueueSize, 0, maxqueue);
    aiPlanSetVariableBool(plan, cTrainPlanUseMultipleBuildings, 0, multibuildings);
    aiPlanSetActive(plan, true);
    return(plan);
}


int planMaintainAt(int unit = -1, int number = 1, int building = -1, int batch = 1, int maxqueue = 1)
{
    int plan = aiPlanCreate("Maintain (specific) "+kbGetProtoUnitName(unit), cPlanTrain);
    aiPlanSetEscrowID(plan, cRootEscrowID);
    aiPlanSetVariableInt(plan, cTrainPlanUnitType, 0, unit);
    aiPlanSetVariableInt(plan, cTrainPlanNumberToMaintain, 0, number);
    aiPlanSetVariableInt(plan, cTrainPlanBatchSize, 0, batch);
    aiPlanSetVariableInt(plan, cTrainPlanBuildingID, 0, building);
    aiPlanSetVariableInt(plan, cTrainPlanMaxQueueSize, 0, maxqueue);
    aiPlanSetVariableBool(plan, cTrainPlanUseMultipleBuildings, 0, false);
    aiPlanSetActive(plan, true);
    return(plan);
}


int planBuild(int building = -1, vector center = cInvalidVector, float center_radius = 30.0, vector location = cInvalidVector, float location_radius = 30.0)
{
    int plan = aiPlanCreate("Build "+kbGetProtoUnitName(building), cPlanBuild);
    aiPlanSetDesiredPriority(plan, 100);
    aiPlanSetEscrowID(plan, cRootEscrowID);
    aiPlanSetAllowUnderAttackResponse(plan, false);
    
    aiPlanSetVariableInt(plan, cBuildPlanBuildingTypeID, 0, building);
    
    aiPlanSetInitialPosition(plan, location);
    
    aiPlanSetVariableVector(plan, cBuildPlanCenterPosition, 0, center);
    aiPlanSetVariableFloat(plan, cBuildPlanCenterPositionDistance, 0, center_radius);
    
    
    aiPlanSetVariableBool(plan, cBuildPlanInfluenceAtBuilderPosition, 0, false);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceBuilderPositionValue, 0, 0.0);
    aiPlanSetVariableFloat(plan, cBuildPlanRandomBPValue, 0, 0.0);
    aiPlanSetVariableFloat(plan, cBuildPlanBuildingBufferSpace, 0, 5.0);
    
    aiPlanSetVariableVector(plan, cBuildPlanInfluencePosition, 0, location);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluencePositionDistance, 0, location_radius);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluencePositionValue, 0, 500.0);
    aiPlanSetVariableInt(plan, cBuildPlanInfluencePositionFalloff, 0, cBPIFalloffLinear);
    
    aiPlanSetNumberVariableValues(plan, cBuildPlanInfluenceUnitTypeID, 4, true);
    aiPlanSetNumberVariableValues(plan, cBuildPlanInfluenceUnitDistance, 4, true);
    aiPlanSetNumberVariableValues(plan, cBuildPlanInfluenceUnitValue, 4, true);
    aiPlanSetNumberVariableValues(plan, cBuildPlanInfluenceUnitFalloff, 4, true);
    
    // Avoid each other
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitTypeID, 0, building);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitDistance, 0, 10.0);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitValue, 0, -20.0);
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitFalloff, 0, cBPIFalloffLinear);
    
    // Avoid trees
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitTypeID, 1, cUnitTypeTree);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitDistance, 1, 10.0);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitValue, 1, -100.0);
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitFalloff, 1, cBPIFalloffLinear);
    
    // Avoid berry bushes
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitTypeID, 2, cUnitTypeAbstractFruit);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitDistance, 2, 10.0);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitValue, 2, -100.0);
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitFalloff, 2, cBPIFalloffLinear);
    
    // Avoid mines
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitTypeID, 3, cUnitTypeMinedResource);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitDistance, 3, 10.0);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitValue, 3, -100.0);
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitFalloff, 3, cBPIFalloffLinear);
    
    return(plan);
}


int planBuildFarFromEachOther(int building = -1, vector center = cInvalidVector, int builder = -1, int num_builders = 1, int pri = 50)
{
    int plan = aiPlanCreate("Build "+kbGetProtoUnitName(building), cPlanBuild);
    aiPlanSetDesiredPriority(plan, pri);
    aiPlanSetEconomy(plan, (kbProtoUnitIsType(cMyID, building, cUnitTypeCountsTowardEconomicScore) == true));
    aiPlanSetMilitary(plan, (kbProtoUnitIsType(cMyID, building, cUnitTypeCountsTowardEconomicScore) == false));
    aiPlanSetEscrowID(plan, cRootEscrowID);
    
    aiPlanSetVariableInt(plan, cBuildPlanBuildingTypeID, 0, building);
    
    aiPlanSetVariableVector(plan, cBuildPlanCenterPosition, 0, center);
    aiPlanSetVariableFloat(plan, cBuildPlanCenterPositionDistance, 0, 100.0);
    
    aiPlanSetNumberVariableValues(plan, cBuildPlanInfluenceUnitTypeID, 4, true);
    aiPlanSetNumberVariableValues(plan, cBuildPlanInfluenceUnitDistance, 4, true);
    aiPlanSetNumberVariableValues(plan, cBuildPlanInfluenceUnitValue, 4, true);
    aiPlanSetNumberVariableValues(plan, cBuildPlanInfluenceUnitFalloff, 4, true);
    
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitTypeID, 0, cUnitTypeWood);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitDistance, 0, 30.0);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitValue, 0, 10.0);
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitFalloff, 0, cBPIFalloffLinear);
    
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitTypeID, 1, cUnitTypeMine);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitDistance, 1, 40.0);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitValue, 1, 300.0);
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitFalloff, 1, cBPIFalloffLinear);
    
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitTypeID, 2, cUnitTypeMine);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitDistance, 2, 10.0);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitValue, 2, -300.0);
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitFalloff, 2, cBPIFalloffNone);
    
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitTypeID, 0, building);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitDistance, 0, 100.0);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluenceUnitValue, 0, -500.0);
    aiPlanSetVariableInt(plan, cBuildPlanInfluenceUnitFalloff, 0, cBPIFalloffNone);
    
    aiPlanSetVariableVector(plan, cBuildPlanInfluencePosition, 0, center);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluencePositionDistance, 0, 100.0);
    aiPlanSetVariableFloat(plan, cBuildPlanInfluencePositionValue, 0, 300.0);
    aiPlanSetVariableInt(plan, cBuildPlanInfluencePositionFalloff, 0, cBPIFalloffLinear);
    
    aiPlanAddUnitType(plan, builder, num_builders, num_builders, num_builders);
    
    aiPlanSetActive(plan, true);
    
    return(plan);
}

