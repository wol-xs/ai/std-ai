/****************************************************************************************************************************************************
    NOTE: this is a header file. Usually, there's no reason to modify header files. Also, there's generally no reason to read header files apart from
    trying to discover and understand the algorithm of certain functions. Be careful if you modify this file.
****************************************************************************************************************************************************/

/****************************************************************************************************************************************************
    Wars of Liberty Standard Header - Queries
    by Thinot "AlistairJah" Mandresy
    Standard Header Files are meant to be reuseable in other AI scripts with minimal modifications.
****************************************************************************************************************************************************/

/****************************************************************************************************************************************************
    We're making use of Quest Vars to make it soooo much simpler for the modder to customize the bots' decks. The steps for creating a deck are
    simpler than ever:
    1. CreateDeck("NameOfTheDeck") -- Creates a deck with 25 free slots. It is possible to create multiple decks as long as they have unique names
    2. AddCardToDeck("NameOfTheDeck", "NameOfTheCard") -- Adds the card to the deck (provided that it's present in the homecity and there's enough 
                                                          slot for placing the card)
    3. ChooseDeck("NameOfTheDeck") -- Given that it's possible to make multiple decks, this functions is for choosing which deck to use in a game.
                                      Should be combined with a random number or a custom logic to allow the bots to choose different decks in 
                                      different games.
    
    It's possible to add a random card to a deck by providing lowercase "random" parameter : AddCardToDeck("NameOfTheDeck", "random")
    And it's possible to choose a random deck by providing lowercase "random" parameter : ChooseDeck("random")
    Therefore, "random" is a reserved word and shall not be used as Deck and/or Card name

    The drawback is that there's now A LOT of reserved Quest Var names:
        "Player"+PlayerID+"Deck"+DeckName+"Index"
        "Player"+PlayerID+"Deck"+DeckName+"Size"
        "Player"+PlayerID+"DeckID"+DeckID+"Size"
        "Player"+PlayerID+"DeckCount"
        "Player"+PlayerID+"Deck"+DeckName+"Exists"
        "Player"+PlayerID+"Deck"+DeckName+"Age"+AgeID+"CardsCount"
        "Player"+PlayerID+"DeckID"+DeckID+" "+CardIndex
        "Player"+PlayerID+"Deck"+DeckName+"ContainsCard"+CardName
    However, everything should be fine as long as the modder doesn't attempt to mess around these names *before* the decks are built and activated.
****************************************************************************************************************************************************/


const int cDefaultDeck = 0;
const int cMaxDeckSize = 25; // Let's not attempt to add more cards than we're allowed to
const int cMaxNumCards = 200; // I think we can safely assume that there will never be a homecity with more than 200 cards


int DeckSize(string deckName = "")
{
    int size = xsQVGet("Player"+cMyID+"Deck"+deckName+"Size");
    return(size);
}


int DeckIndex(string deckName = "")
{
    int index = xsQVGet("Player"+cMyID+"Deck"+deckName+"Index");
    return(index);
}


bool SetDeckIndex(string deckName = "", int index = -1)
{
    xsQVSet("Player"+cMyID+"Deck"+deckName+"Index", index);
    return(true);
}


string DeckStatus(string deckName = "")
{
    if (xsQVGet("Player"+cMyID+"Deck"+deckName+"Exists") == 0)
    {
        return("NONEXISTENT"); // The deck does not exist
    }
    
    if (DeckSize(deckName) >= cMaxDeckSize)
    {
        return("FULL"); // The deck exists and is full of cards
    }
    
    return("AVAILABLE"); // The deck exists and has available slots for more cards
}


bool IncrementDeckSize(string deckName = "")
{
    if (deckName == "" || DeckStatus(deckName) == "NONEXISTENT")
    {
        return(false);
    }
    
    if (DeckSize(deckName) >= cMaxDeckSize)
    {
        return(false);
    }
    
    xsQVSet("Player"+cMyID+"Deck"+deckName+"Size", xsQVGet("Player"+cMyID+"Deck"+deckName+"Size") + 1);
    xsQVSet("Player"+cMyID+"DeckID"+DeckIndex(deckName)+"Size", xsQVGet("Player"+cMyID+"Deck"+deckName+"Size"));
    return(true);
}


int DeckCount(void)
{
    int count = xsQVGet("Player"+cMyID+"DeckCount");
    return(count);
}


void IncrementDeckCount(void)
{
    xsQVSet("Player"+cMyID+"DeckCount", xsQVGet("Player"+cMyID+"DeckCount") + 1);
}


string CreateDeck(string deckName = "")
{
    if (deckName == "" || deckName == "random")
    {
        return("FAILURE_FORBIDDEN"); // We don't allow modders to use empty deck name or 'random' as deck name
    }
    
    if (DeckStatus(deckName) != "NONEXISTENT")
    {
        return("FAILURE_DUPLICATE"); // Each deck must have a unique name
    }
    
    xsQVSet("Player"+cMyID+"Deck"+deckName+"Exists", 1);
    SetDeckIndex(deckName, DeckCount());
    IncrementDeckCount();
    return("SUCCESS"); // We've successfully created the deck. It's currently empty.
}


bool DeckContainsCard(string deckName = "", string cardName = "")
{
    // Check if the deck already contains the card
    int retval = xsQVGet("Player"+cMyID+"Deck"+deckName+"ContainsCard"+cardName);
    return(retval != 0);
}


int AgeXCardsCount(int ageX = -1, string deckName = "")
{
    if (ageX <= -1 || ageX >= cAge5)
    {
        // Invalid or unsupported age -- return -1 (it means 'invalid' or 'undefined')
        return(-1);
    }
    
    int count = 0;
    if (deckName == "")
    {
        // Empty deck name -- return the total number of age X cards in the entire homecity
        for(card = 0 ; < aiHCCardsGetTotal())
        {
            if (aiHCCardsGetCardAgePrereq(card) == ageX)
            {
                count++;
            }
        }
        return(count);
    }
    
    if (DeckStatus(deckName) == "NONEXISTENT")
    {
        // Non-empty deck name, non-existent deck -- return -1 (it means 'invalid' or 'undefined')
        return(-1);
    }
    
    count = xsQVGet("Player"+cMyID+"Deck"+deckName+"Age"+ageX+"CardsCount");
    return(count);
}


bool IncrementAgeXCardsCount(int ageX = -1, string deckName = "")
{
    if (ageX <= -1 || ageX >= cAge5)
    {
        return(false);
    }
    
    if (deckName == "" || DeckStatus(deckName) == "NONEXISTENT")
    {
        return(false);
    }
    
    if (AgeXCardsCount(ageX, deckName) >= 10)
    {
        return(false);
    }
    
    xsQVSet("Player"+cMyID+"Deck"+deckName+"Age"+ageX+"CardsCount", xsQVGet("Player"+cMyID+"Deck"+deckName+"Age"+ageX+"CardsCount") + 1);
    return(true);
}


string AddCardToDeck(string deckName = "", string cardName = "")
{
    if (deckName == "" || deckName == "random")
    {
        return("FAILURE_FORBIDDEN"); // We don't allow modders to add cards to a random deck
    }
    
    if (DeckStatus(deckName) == "NONEXISTENT")
    {
        return("FAILURE_NONEXISTENT"); // We force modders to create a deck before adding cards
    }
    
    if (DeckStatus(deckName) == "FULL")
    {
        return("OUTOFBOUNDS"); // We don't allow modders to add more cards than the maximum allowed (cMaxDeckSize)
    }
    
    if (DeckContainsCard(deckName, cardName) && cardName != "" && cardName != "random")
    {
        return("FAILURE_DUPLICATE"); // Let's avoid incrementing the card count for nothing
    }
    
    if (cardName == "" || cardName == "random")
    {
        // The modder told us to add a random card to this deck, so let's... try ^_^'
        static int rand_cards = -1;
        if (rand_cards == -1)
            rand_cards = xsArrayCreateInt(cMaxNumCards, -1, "Random Cards Buffer");
        int i = 0;
        int j = 0;
        for(i = 0 ; < aiHCCardsGetTotal())
        {
            if (DeckContainsCard(deckName, kbGetTechName(aiHCCardsGetCardTechID(i))))
            {
                continue;
            }
            
            if (AgeXCardsCount(aiHCCardsGetCardAgePrereq(i), deckName) >= 10)
            {
                continue;
            }
            
            xsArraySetInt(rand_cards, j, i);
            j++;
        }
        xsArraySetInt(rand_cards, j, -1); // Let this be the rightmost bounds
        
        int rand_card = xsArrayGetInt(rand_cards, aiRandInt(j));
        string rand_card_name = kbGetTechName(aiHCCardsGetCardTechID(rand_card));
        int rand_card_age = aiHCCardsGetCardAgePrereq(rand_card);
            
        xsQVSet("Player"+cMyID+"DeckID"+DeckIndex(deckName)+" "+DeckSize(deckName), rand_card);
        IncrementDeckSize(deckName);
        IncrementAgeXCardsCount(rand_card_age, deckName);
        xsQVSet("Player"+cMyID+"Deck"+deckName+"ContainsCard"+rand_card_name, 1);
        
        return("SUCCESS_RANDOM");
    }
    
    
    for(card = 0 ; < aiHCCardsGetTotal())
    {
        if (DeckContainsCard(deckName, kbGetTechName(aiHCCardsGetCardTechID(card))))
        {
            return("FAILURE_DUPLICATE");
        }
        
        string card_tech_name = kbGetTechName(aiHCCardsGetCardTechID(card));
        if (card_tech_name == cardName)
        {
            int card_age = aiHCCardsGetCardAgePrereq(card);
            if (AgeXCardsCount(card_age, deckName) >= 10)
            {
                return("FAILURE_NOMORESLOTFORAGE");
            }
            
            xsQVSet("Player"+cMyID+"DeckID"+DeckIndex(deckName)+" "+DeckSize(deckName), card);
            IncrementDeckSize(deckName);
            IncrementAgeXCardsCount(card_age, deckName);
            xsQVSet("Player"+cMyID+"Deck"+deckName+"ContainsCard"+card_tech_name, 1);
            
            return("SUCCESS_SPECIFIC");
        }
    }
    
    return("This should be impossible... If this shows up, please check string AddCardToDeck() in headers/aai_decks.xs");
}


int DeckCard(string deckName = "", int index = -1)
{
    int id = xsQVGet("Player"+cMyID+"DeckID"+DeckIndex(deckName)+" "+index);
    return(id);
}


void ChooseDeck(string deckName = "")
{
    if (deckName == "" || deckName == "random")
    {
        // The modder told us to activate a random deck (both empty name and 'random' mean random)
        int picked_deck = aiRandInt(DeckCount());
        for(i = 0 ; < xsQVGet("Player"+cMyID+"DeckID"+picked_deck+"Size"))
        {
            aiHCDeckAddCardToDeck(cDefaultDeck, xsQVGet("Player"+cMyID+"DeckID"+picked_deck+" "+i));
        }
        aiHCDeckActivate(cDefaultDeck);
        return;
    }
    
    // The modder told us to activate a specific deck
    for(i = 0 ; < xsQVGet("Player"+cMyID+"DeckID"+DeckIndex(deckName)+"Size"))
    {
        aiHCDeckAddCardToDeck(cDefaultDeck, DeckCard(deckName, i));
    }
    aiHCDeckActivate(cDefaultDeck);
}

