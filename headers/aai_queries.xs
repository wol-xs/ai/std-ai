/****************************************************************************************************************************************************
    NOTE: this is a header file. Usually, there's no reason to modify header files. Also, there's generally no reason to read header files apart from
    trying to discover and understand the algorithm of certain functions. Be careful if you modify this file.
****************************************************************************************************************************************************/

/****************************************************************************************************************************************************
    Wars of Liberty Standard Header - Queries
    by Thinot "AlistairJah" Mandresy
    Standard Header Files are meant to be reuseable in other AI scripts with minimal modifications.
****************************************************************************************************************************************************/

/****************************************************************************************************************************************************
    Query functions are used for searching units on the terrain according to certain criters/filters. Query functions can also be used to count units
    Each query function (except countUnitsByLocation) contains static data (it means that the data remains available for multiple calls) to make it 
    possible to iterate through the queried units by using the same function. To refresh/reset the static data, use index <= 0. Example:
    findUnit1(cUnitTypeAbstractVillager, cMyID, -1) -- this will refresh the static data, find all AbstractVillager and return a random one if any is
    found or return -1 if none is found.
    findUnit1(cUnitTypeAbstractVillager, cMyID, 0) -- this will refresh the static data, find all AbstractVillager and return the first in the result
    if any is found or return -1 if none is found. After this, if you call findUnit1() again in a different index (1 or 2 or 3 or anything >= 1), the
    static data will not be refreshed anymore and the function will simply return the index-th unit in the result (or -1 if no unit is found). If you
    want to refresh again, just call findUnit1() at index 0 (or negative index).
    The same thing applies to all query functions (except countUnitsByLocation() which always returns up to date unit count).
    
      /!\   Due to the static data, be careful when you use the same function in different depth of iteration:
     / ! \  
    /  !  \ DO NOT DO THIS:
                for(i = 0 ; < 10)
                {
                    int level_1_unit = findUnit1(cUnitTypeUnit, cMyID, i);
                    for(j = 0 ; < 10)
                    {
                        int level_2_unit = findUnit1(cUnitTypeUnit, cMyID, j);
                    }
                }
                
            Instead, findUnit2() and findUnit3() have been made for this case:
                for(i = 0 ; < 10)
                {
                    int level_1_unit = findUnit1(cUnitTypeUnit, cMyID, i);
                    for(j = 0 ; < 10)
                    {
                        int level_2_unit = findUnit2(cUnitTypeUnit, cMyID, j);
                    }
                }
****************************************************************************************************************************************************/


int findUnit1(int unit_type = -1, int owner = cMyID, int index = -1)
{
    static int q = -1;
    if (q == -1)
        q = kbUnitQueryCreate("q1");
    
    if (index <= 0)
    {
        kbUnitQueryResetResults(q);
        kbUnitQuerySetState(q, cUnitStateAlive);
        kbUnitQuerySetIgnoreKnockedOutUnits(q, true);
        kbUnitQuerySetUnitType(q, unit_type);
        if (owner >= 1000)
        {
            kbUnitQuerySetPlayerID(q, -1, false);
            kbUnitQuerySetPlayerRelation(q, owner);
        }
        else
        {
            kbUnitQuerySetPlayerRelation(q, -1);
            kbUnitQuerySetPlayerID(q, owner, false);
        }
        int count = kbUnitQueryExecute(q);
        if (index <= -1)
            return(kbUnitQueryGetResult(q, aiRandInt(count)));
    }
    
    return(kbUnitQueryGetResult(q, index));
}


int findUnit2(int unit_type = -1, int owner = cMyID, int index = -1)
{
    static int q = -1;
    if (q == -1)
        q = kbUnitQueryCreate("q2");
    
    if (index <= 0)
    {
        kbUnitQueryResetResults(q);
        kbUnitQuerySetState(q, cUnitStateAlive);
        kbUnitQuerySetIgnoreKnockedOutUnits(q, true);
        kbUnitQuerySetUnitType(q, unit_type);
        if (owner >= 1000)
        {
            kbUnitQuerySetPlayerID(q, -1, false);
            kbUnitQuerySetPlayerRelation(q, owner);
        }
        else
        {
            kbUnitQuerySetPlayerRelation(q, -1);
            kbUnitQuerySetPlayerID(q, owner, false);
        }
        int count = kbUnitQueryExecute(q);
        if (index <= -1)
            return(kbUnitQueryGetResult(q, aiRandInt(count)));
    }
    
    return(kbUnitQueryGetResult(q, index));
}


int findUnit3(int unit_type = -1, int owner = cMyID, int index = -1)
{
    static int q = -1;
    if (q == -1)
        q = kbUnitQueryCreate("q3");
    
    if (index <= 0)
    {
        kbUnitQueryResetResults(q);
        kbUnitQuerySetState(q, cUnitStateAlive);
        kbUnitQuerySetIgnoreKnockedOutUnits(q, true);
        kbUnitQuerySetUnitType(q, unit_type);
        if (owner >= 1000)
        {
            kbUnitQuerySetPlayerID(q, -1, false);
            kbUnitQuerySetPlayerRelation(q, owner);
        }
        else
        {
            kbUnitQuerySetPlayerRelation(q, -1);
            kbUnitQuerySetPlayerID(q, owner, false);
        }
        int count = kbUnitQueryExecute(q);
        if (index <= -1)
            return(kbUnitQueryGetResult(q, aiRandInt(count)));
    }
    
    return(kbUnitQueryGetResult(q, index));
}


int findUnitByLocation1(int unit_type = -1, int owner = cMyID, vector location = cInvalidVector, float radius = 0.0, int index = -1)
{
    static int q = -1;
    if (q == -1)
        q = kbUnitQueryCreate("ql1");
    
    if (index <= 0)
    {
        kbUnitQueryResetResults(q);
        kbUnitQuerySetState(q, cUnitStateAlive);
        kbUnitQuerySetIgnoreKnockedOutUnits(q, true);
        kbUnitQuerySetAscendingSort(q, true);
        kbUnitQuerySetUnitType(q, unit_type);
        kbUnitQuerySetPosition(q, location);
        kbUnitQuerySetMaximumDistance(q, radius);
        if (owner >= 1000)
        {
            kbUnitQuerySetPlayerID(q, -1, false);
            kbUnitQuerySetPlayerRelation(q, owner);
        }
        else
        {
            kbUnitQuerySetPlayerRelation(q, -1);
            kbUnitQuerySetPlayerID(q, owner, false);
        }
        int count = kbUnitQueryExecute(q);
        if (index <= -1)
            return(kbUnitQueryGetResult(q, aiRandInt(count)));
    }
    
    return(kbUnitQueryGetResult(q, index));
}


int findUnitByLocation2(int unit_type = -1, int owner = cMyID, vector location = cInvalidVector, float radius = 0.0, int index = -1)
{
    static int q = -1;
    if (q == -1)
        q = kbUnitQueryCreate("ql2");
    
    if (index <= 0)
    {
        kbUnitQueryResetResults(q);
        kbUnitQuerySetState(q, cUnitStateAlive);
        kbUnitQuerySetIgnoreKnockedOutUnits(q, true);
        kbUnitQuerySetAscendingSort(q, true);
        kbUnitQuerySetUnitType(q, unit_type);
        kbUnitQuerySetPosition(q, location);
        kbUnitQuerySetMaximumDistance(q, radius);
        if (owner >= 1000)
        {
            kbUnitQuerySetPlayerID(q, -1, false);
            kbUnitQuerySetPlayerRelation(q, owner);
        }
        else
        {
            kbUnitQuerySetPlayerRelation(q, -1);
            kbUnitQuerySetPlayerID(q, owner, false);
        }
        int count = kbUnitQueryExecute(q);
        if (index <= -1)
            return(kbUnitQueryGetResult(q, aiRandInt(count)));
    }
    
    return(kbUnitQueryGetResult(q, index));
}


int findUnitByLocation3(int unit_type = -1, int owner = cMyID, vector location = cInvalidVector, float radius = 0.0, int index = -1)
{
    static int q = -1;
    if (q == -1)
        q = kbUnitQueryCreate("ql3");
    
    if (index <= 0)
    {
        kbUnitQueryResetResults(q);
        kbUnitQuerySetState(q, cUnitStateAlive);
        kbUnitQuerySetIgnoreKnockedOutUnits(q, true);
        kbUnitQuerySetAscendingSort(q, true);
        kbUnitQuerySetUnitType(q, unit_type);
        kbUnitQuerySetPosition(q, location);
        kbUnitQuerySetMaximumDistance(q, radius);
        if (owner >= 1000)
        {
            kbUnitQuerySetPlayerID(q, -1, false);
            kbUnitQuerySetPlayerRelation(q, owner);
        }
        else
        {
            kbUnitQuerySetPlayerRelation(q, -1);
            kbUnitQuerySetPlayerID(q, owner, false);
        }
        int count = kbUnitQueryExecute(q);
        if (index <= -1)
            return(kbUnitQueryGetResult(q, aiRandInt(count)));
    }
    
    return(kbUnitQueryGetResult(q, index));
}


int countUnitsByLocation(int unit_type = -1, int owner = cMyID, vector location = cInvalidVector, float radius = 0.0)
{
    static int q = -1;
    if (q == -1)
        q = kbUnitQueryCreate("cl");
    
    kbUnitQueryResetResults(q);
    kbUnitQuerySetState(q, cUnitStateAlive);
    kbUnitQuerySetIgnoreKnockedOutUnits(q, true);
    kbUnitQuerySetUnitType(q, unit_type);
    kbUnitQuerySetPosition(q, location);
    kbUnitQuerySetMaximumDistance(q, radius);
    if (owner >= 1000)
    {
        kbUnitQuerySetPlayerID(q, -1, false);
        kbUnitQuerySetPlayerRelation(q, owner);
    }
    else
    {
        kbUnitQuerySetPlayerRelation(q, -1);
        kbUnitQuerySetPlayerID(q, owner, false);
    }
    
    return(kbUnitQueryExecute(q));
}

