rule MakeDefaultDeck active minInterval 60
{
	// We'll give the modders 1 minute to set up and activate their own decks for their AIs
	// If, after that, we still don't detect any active deck, we will make a deck out of completely random cards
	xsDisableSelf();
	
	if (xsQVGet("Player"+cMyID+"DeckCount") >= 1)
		return;
	
	for(i = 0 ; < 25)
	{
		AddCardToDeck("Default", "random");
	}
	xsEnableRule("ActivateDeck");
}


rule ActivateDeck inactive minInterval 1
{
	xsDisableSelf();
	ChooseDeck("Default");
}

