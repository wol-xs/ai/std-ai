rule MonitorExplorationLand active minInterval 10 runImmediately
{
	// This is a cleaned up version of the vanilla rule exploreMonitor
	// I am too lazy to copy-paste the comments xD -- sorry
    const int cExploreModeStart = 0;
    const int cExploreModeNugget = 1;
    const int cExploreModeStaff = 2;
    const int cExploreModeExplore = 3;
    
    static int explorePlan = -1;
	static int defensePlan = -1;
    static int exploreMode = cExploreModeStart;
    static int age2Time = -1;
    static int nextStaffTime = -1;
    
    if (age2Time < 0 && kbGetAge() >= cAge2)
        age2Time = xsGetTime();
    
    switch(exploreMode)
    {
        case cExploreModeStart:
        {
            if (aiPlanGetState(explorePlan) == -1)
            {
                explorePlan = aiPlanCreate("ExploreLand", cPlanExplore);
                aiPlanSetDesiredPriority(explorePlan, 75);
                aiPlanAddUnitType(explorePlan, cUnitTypeHero, 1, 1, 1);
                aiPlanAddUnitType(explorePlan, cUnitTypeLogicalTypeScout, 1, 6, 10);
                aiPlanSetVariableBool(explorePlan, cExplorePlanOkToGatherNuggets, 0, true);
                exploreMode = cExploreModeNugget;
                aiPlanSetEscrowID(explorePlan, cRootEscrowID);
                aiPlanSetBaseID(explorePlan, kbBaseGetMainID(cMyID));
                aiPlanSetVariableBool(explorePlan, cExplorePlanDoLoops, 0, true);
                aiPlanSetVariableInt(explorePlan, cExplorePlanNumberOfLoops, 0, 1);
                aiPlanSetActive(explorePlan, true); 
            }
            else
            {
                exploreMode = cExploreModeNugget;
            }
            break;
        }
        case cExploreModeNugget:
        {
            if (age2Time >= 0)
            {
                if (xsGetTime() - age2Time > 180000 && aiPlanGetState(explorePlan) != cPlanStateClaimNugget)
                {
                    if (defensePlan == -1)
                    {
                        defensePlan = aiPlanCreate("ExplorerRTB", cPlanDefend);
                        aiPlanAddUnitType(defensePlan, cUnitTypeHero, 1, 1, 1);
                        aiPlanSetVariableVector(defensePlan, cDefendPlanDefendPoint, 0, kbBaseGetLocation(cMyID, kbBaseGetMainID(cMyID)) + 
                                               xsVectorNormalize(aiRandLocation() - kbBaseGetLocation(cMyID, kbBaseGetMainID(cMyID))) * 20.0);
                        aiPlanSetVariableFloat(defensePlan, cDefendPlanEngageRange, 0, 20.0);
                        aiPlanSetVariableBool(defensePlan, cDefendPlanPatrol, 0, false);
                        aiPlanSetVariableFloat(defensePlan, cDefendPlanGatherDistance, 0, 20.0);
                        aiPlanSetInitialPosition(defensePlan, kbBaseGetLocation(cMyID, kbBaseGetMainID(cMyID)));
                        aiPlanSetUnitStance(defensePlan, cUnitStanceDefensive);
                        aiPlanSetVariableInt(defensePlan, cDefendPlanRefreshFrequency, 0, 30);
                        aiPlanSetVariableInt(defensePlan, cDefendPlanAttackTypeID, 0, cUnitTypeUnit);
                        aiPlanSetDesiredPriority(defensePlan, 90);
                        aiPlanSetActive(defensePlan, true);      
                    }
                    
                    aiPlanAddUnitType(explorePlan, cUnitTypeHero, 0, 0, 0);
                    aiPlanAddUnitType(explorePlan, cUnitTypeLogicalTypeValidSharpshoot, 1, 1, 1);
                    aiPlanSetNoMoreUnits(explorePlan, false);
                    aiPlanSetVariableInt(explorePlan, cExplorePlanNumberOfLoops, 0, 0);
                    aiPlanSetVariableBool(explorePlan, cExplorePlanDoLoops, 0, false);
                    exploreMode = cExploreModeStaff;
                    nextStaffTime = xsGetTime() + 120000;
                }
            }
            break;
        }
        case cExploreModeStaff:
        {
            aiPlanSetNoMoreUnits(explorePlan, true);
            exploreMode = cExploreModeExplore;
            break;
        }
        case cExploreModeExplore:
        {
            if (xsGetTime() > nextStaffTime)
            {
                aiPlanSetNoMoreUnits(explorePlan, false);
                nextStaffTime = xsGetTime() + 120000;
                exploreMode = cExploreModeStaff;
            }
            break;
        }
    }
}


rule MonitorExplorationWater inactive minInterval 10
{
	// TODO
}


