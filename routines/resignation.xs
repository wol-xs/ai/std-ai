

void EventResignResponseReceived(int response = -1)
{
    if (response == 0)
    {
        xsSetRuleMinInterval("MonitorResignDecision", 300);
        xsEnableRule("MonitorResignDecision");
        return;
    }
    
    int receiver = 0;
    int lowest_score = 99999999;
    for (player = 1 ; < cNumberPlayers)
    {
        if (player == cMyID)
            continue;
        if (kbGetPlayerTeam(cMyID) != kbGetPlayerTeam(player))
            continue;
        if (kbIsPlayerHuman(player) == false)
            continue;
        if (lowest_score < aiGetScore(player))
            continue;
        lowest_score = aiGetScore(player);
        receiver = player;
    }
    if (receiver == 0)
    {
        lowest_score = 99999999;
        for (player = 1 ; < cNumberPlayers)
        {
            if (player == cMyID)
                continue;
            if (kbGetPlayerTeam(cMyID) != kbGetPlayerTeam(player))
                continue;
            if (lowest_score < aiGetScore(player))
                continue;
            lowest_score = aiGetScore(player);
            receiver = player;
        }
    }
    
    if (receiver >= 1)
    {
        aiTribute(receiver, cResourceFood, kbResourceGet(cResourceFood));
        aiTribute(receiver, cResourceWood, kbResourceGet(cResourceWood));
        aiTribute(receiver, cResourceGold, kbResourceGet(cResourceGold));
    }
    
    aiResign();
}


rule MonitorResignDecision active minInterval 5
{
    int human_allies = 0;
    int human_allies_still_in = 0;
    for(player = 1 ; < cNumberPlayers)
    {
        if (kbIsPlayerHuman(player) == false)
            continue;
        if (kbIsPlayerAlly(player) == false)
            continue;
        human_allies++;
        if (kbHasPlayerLost(player) == true)
            continue;
        human_allies_still_in++;
    }
    
    if ((human_allies >= 1) && (human_allies_still_in == 0))
    {
        for(player = 1 ; < cNumberPlayers)
        {
            if (kbIsPlayerHuman(player) == false)
                continue;
            aiChat(player, "Hey "+kbGetPlayerName(player)+", in case this bot resigned in the middle of a Diplomacy game, it's normal -- diplomacy support hasn't been implemented yet.");
            aiChat(player, "Stay tuned for updates ^_^ -- Alistair");
        }
        EventResignResponseReceived(1);
        xsDisableSelf();
        return;
    }
    
    if (xsGetTime() < 300000)
        return;
    
    if (kbGetPop() >= 30)
        return;
    
    int total_pop = 0;
    int enemy_pop = 0;
    int num_enemy = 0;
    for(player = 1 ; < cNumberPlayers)
    {
        if (kbHasPlayerLost(player) == true)
            continue;
        if (player == cMyID)
            total_pop = total_pop + kbUnitCount(player, cUnitTypeUnit, cUnitStateAlive);
        if (kbIsPlayerAlly(player) == true)
            continue;
        enemy_pop = enemy_pop + kbUnitCount(player, cUnitTypeUnit, cUnitStateAlive);
        num_enemy++;
    }
    
    if (num_enemy == 0) num_enemy = 1;
    
    if (((enemy_pop/num_enemy) / total_pop) > 10)
    {
        if (human_allies == 0)
        {
            aiAttemptResign(cAICommPromptToEnemyMayIResign);
            EventResignResponseReceived(0);
            return;
        }
        static bool complain = true;
        if (complain)
        {
            complain = false;
            for(player = 1 ; < cNumberPlayers)
            {
                if (kbIsPlayerHuman(player) == false)
                    continue;
                if (kbIsPlayerAlly(player) == false)
                    continue;
                aiCommsSendStatement(player, cAICommPromptToAllyImReadyToQuit);
            }
        }
    }
    
    if (((enemy_pop/num_enemy) / total_pop) <= 4)
        return;
    if ((kbUnitCount(cMyID, cUnitTypeMaoriPa, cUnitStateABQ) >= 1) || (kbUnitCount(cMyID, cUnitTypePaPorter, cUnitStateABQ) >= 1))
        return;
    if (human_allies >= 1)
        return;
    aiAttemptResign(cAICommPromptToEnemyMayIResign);
    EventResignResponseReceived(0);
}

