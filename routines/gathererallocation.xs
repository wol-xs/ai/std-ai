

float aiGetCurrentResourceNeeds(int resource = -1)
{
    if (resource <= -1 || resource >= 8)
    {
        return(0.0);
    }
    
    float needs = 0.0;
    
    for(i = 0 ; < aiPlanGetNumber(-1, -1, true))
    {
        int plan = aiPlanGetIDByIndex(-1, -1, true, i);
        int plan_type = aiPlanGetType(plan);
        
        if (plan_type == cPlanBuild && aiPlanGetState(plan) != cPlanStateBuild)
        {
            int building = aiPlanGetVariableInt(plan, cBuildPlanBuildingTypeID, 0);
            needs = needs + kbUnitCostPerResource(building, resource);
            continue;
        }
        
        if (plan_type == cPlanTrain)
        {
            int unit = aiPlanGetVariableInt(plan, cTrainPlanUnitType, 0);
            if (aiPlanGetVariableInt(plan, cTrainPlanNumberToTrain, 0) >= 1)
            {
                needs = needs + kbUnitCostPerResource(unit, resource) * aiPlanGetVariableInt(plan, cTrainPlanNumberToTrain, 0);
                continue;
            }
            
            if (aiPlanGetVariableInt(plan, cTrainPlanNumberToMaintain, 0) >= 1)
            {
                int goal = aiPlanGetVariableInt(plan, cTrainPlanNumberToMaintain, 0);
                int curr = kbUnitCount(cMyID, unit, cUnitStateABQ);
                int shrt = goal - curr;
                if (shrt <= 0)
                {
                    shrt = 0;
                }
                needs = needs + kbUnitCostPerResource(unit, resource) * shrt;
                continue;
            }
            
            continue;
        }
        
        if (plan_type == cPlanResearch && aiPlanGetState(plan) != cPlanStateResearch)
        {
            int tech = aiPlanGetVariableInt(plan, cResearchPlanTechID, 0);
            needs = needs + kbTechCostPerResource(tech, resource);
            continue;
        }
    }
    
    return(needs);
}


rule AllocateGatherers
active
minInterval 10
runImmediately
{
    const float cMinAmountWoodPerVillager = 50.0;
    float handicap = kbGetPlayerHandicap(cMyID);
    float prct_gold = 0.0;
    float prct_wood = 0.0;
    float prct_food = 1.0;
    float prct_totl = 1.0;
    float ivtr_gold = kbResourceGet(cResourceGold);
    float ivtr_wood = kbResourceGet(cResourceWood);
    float ivtr_food = kbResourceGet(cResourceFood);
    float ivtr_totl = ivtr_gold + ivtr_wood + ivtr_food;
    float need_gold = aiGetCurrentResourceNeeds(cResourceGold) - ivtr_gold;
    float need_wood = aiGetCurrentResourceNeeds(cResourceWood) - ivtr_wood;
    float need_food = aiGetCurrentResourceNeeds(cResourceFood) - ivtr_food;
    int vill_count = kbUnitCount(cMyID, cUnitTypeAbstractVillager, cUnitStateAlive);
    if (need_gold < 0.0)
    {
        need_gold = 0.0;
    }
    if (need_wood < 0.0)
    {
        need_wood = 0.0;
    }
    if (need_food < 0.0)
    {
        need_food = 0.0;
    }
    float need_totl = need_gold + need_wood + need_food;
    if (need_totl <= 1)
    {
        prct_gold = 1.0 - ivtr_gold / ivtr_totl;
        prct_wood = 1.0 - ivtr_wood / ivtr_totl;
        prct_food = 1.0 - ivtr_food / ivtr_totl;
    }
    else
    {
        prct_gold = need_gold / need_totl;
        prct_wood = need_wood / need_totl;
        prct_food = need_food / need_totl;
    }
    if (kbGetAmountValidResources(kbBaseGetMainID(cMyID), cResourceWood, cAIResourceSubTypeEasy, 120.0) < cMinAmountWoodPerVillager * vill_count)
    {
        prct_wood = 0.0;
    }
    
    prct_totl = prct_gold + prct_wood + prct_food;
    prct_gold = prct_gold / prct_totl;
    prct_wood = prct_wood / prct_totl;
    prct_food = prct_food / prct_totl;
    
    aiSetResourceGathererPercentage(cResourceGold, prct_gold, false, cRGPScript);
    aiSetResourceGathererPercentage(cResourceWood, prct_wood, false, cRGPScript);
    aiSetResourceGathererPercentage(cResourceFood, prct_food, false, cRGPScript);
    
    aiNormalizeResourceGathererPercentages(cRGPScript);
}

